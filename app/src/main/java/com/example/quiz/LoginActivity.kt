package com.example.quiz

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.quiz.R
import com.example.quiz.model.AuthRequest
import com.example.quiz.model.AuthResponse
import com.example.quiz.model.QuestionResponse
import com.example.quiz.model.RetrofitBuilder
import com.example.quiz.utils.MockRemote
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val token = MockRemote.getDefaults("token", this)

        token?.let {
            if (token.isNotEmpty()) {
                MockRemote.token = token
                val intent = Intent(this, QuestionsActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

        loginBtn.setOnClickListener {
            val auth =
                AuthRequest(PhoneNumberEditText.text.toString(), PasswordEditText.text.toString())
            val call = RetrofitBuilder.apiService.authentication(auth)
            call.enqueue(object : Callback<AuthResponse> {
                override fun onFailure(call: Call<AuthResponse>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<AuthResponse>,
                    response: Response<AuthResponse>
                ) {
                    response.body().let {
                        if (response.code() == 200 || response.code() == 202) {
                            if (response.body()?.statusCode == 1) {
                                MockRemote.setDefaults(
                                    "token",
                                    response.body()?.token,
                                    this@LoginActivity
                                )
                                MockRemote.token = response.body()?.token!!
                                val intent =
                                    Intent(this@LoginActivity, QuestionsActivity::class.java)
                                startActivity(intent)
                                finish()
                            } else {
                                Toast.makeText(
                                    this@LoginActivity,
                                    "نام کاربری و یا رمز عبور اشتباه است",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }
            })
        }
    }
}
