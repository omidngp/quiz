package com.example.quiz

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.quiz.databinding.ActivityQuestionsBinding
import com.example.quiz.utils.MockRemote
import com.example.quiz.R
import com.example.quiz.model.*
import kotlinx.android.synthetic.main.activity_questions.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class QuestionsActivity : AppCompatActivity() {

    // The first answer is the correct one.  We randomize the answers before showing the text.
    // All questions must have four answers.  We'd want these to contain references to string
    // resources so we could internationalize. (or better yet, not define the questions in code...)
    private lateinit var questions: MutableList<Question>



    var currentQuestion= Question(
            id = 0, text = "", answers = listOf("","","",""), correctAnswer = 1, selectedAnswer = 1, remainingTime = 0
    )
    var answers = mutableListOf("", "", "", "")
    private var questionIndex = 0
    private var selectedId: Int? = -1
    var remainingTime: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityQuestionsBinding>(
            this, R.layout.activity_questions)

        val call = RetrofitBuilder.apiService.getQuestions(QuestionRequest(10))
        call.enqueue(object : Callback<QuestionResponse> {
            override fun onFailure(call: Call<QuestionResponse>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<QuestionResponse>,
                response: Response<QuestionResponse>
            ) {
                response.body().let {
                    questions = MockRemote.mapQuestionEntityToQuestion(response.body()?.data)
                    binding.invalidateAll()
                    randomizeQuestions()
                }
            }
        })



        // Bind this fragment class to the layout
        binding.game = this


        binding.nextBtn.setOnClickListener {
            countDownTimer.cancel()
            val question = Question(
                id = questions[questionIndex].id,
                text = questions[questionIndex].text,
                answers = questions[questionIndex].answers,
                correctAnswer = questions[questionIndex].correctAnswer,
                selectedAnswer = selectedId,
                remainingTime = remainingTime
            )
            questions[questionIndex] = question
            questionIndex++
            binding.invalidateAll()
            setQuestion()
        }

        binding.previousBtn.setOnClickListener{
            countDownTimer.cancel()
            val question = Question(
                id = questions[questionIndex].id,
                text = questions[questionIndex].text,
                answers = questions[questionIndex].answers,
                correctAnswer = questions[questionIndex].correctAnswer,
                selectedAnswer = selectedId,
                remainingTime = remainingTime
            )
            questions[questionIndex] = question
            questionIndex--
            binding.invalidateAll()
            setQuestion()
        }

        // Set the onClickListener for the submitButton
        binding.submitButton.setOnClickListener { view: View ->

            val examList = mutableListOf<ExamRequest>()
            var correctCount: Int = 0
            var wrongCount: Int = 0
            var unsolvedCount: Int = 0
            for(question in questions){
                val examRequest = ExamRequest(
                    question.id,
                    question.selectedAnswer ?: -1
                )
                examList.add(examRequest)
                when(question.selectedAnswer){
                    null -> unsolvedCount++
                    question.correctAnswer -> correctCount++
                    else -> wrongCount++
                }

            }

            val call2 = RetrofitBuilder.apiService.sendExam(examList)
            call2.enqueue(object : Callback<Void> {
                override fun onFailure(call: Call<Void>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<Void>,
                    response: Response<Void>
                ) {
                    val intent = Intent(this@QuestionsActivity, ResultsActivity::class.java)
                    intent.putExtra("TOTAL", questions.size)
                    intent.putExtra("CORRECT", correctCount)
                    intent.putExtra("WRONG", wrongCount)
                    intent.putExtra("UNSOLVED", unsolvedCount)
                    startActivity(intent)
                    finish()
                }
            })

        }
    }

    fun setCheckedId(checkedId: Int?){
        if(remainingTime == 0 && checkedId != null){
            Toast.makeText(this, "زمان پاسخ به سوال به اتمام رسیده است", Toast.LENGTH_SHORT).show()
            return
        }
        this.selectedId = checkedId
        resetAllCardViews()

        when(checkedId){
            1 -> firstAnswer.strokeColor = resources.getColor(R.color.colorPrimary)
            2 -> secondAnswer.strokeColor = resources.getColor(R.color.colorPrimary)
            3 -> thirdAnswer.strokeColor = resources.getColor(R.color.colorPrimary)
            4 -> fourthAnswer.strokeColor = resources.getColor(R.color.colorPrimary)
            else -> return
        }
    }

    private fun resetAllCardViews() {
        firstAnswer.strokeColor = resources.getColor(R.color.gray)
        secondAnswer.strokeColor = resources.getColor(R.color.gray)
        thirdAnswer.strokeColor = resources.getColor(R.color.gray)
        fourthAnswer.strokeColor = resources.getColor(R.color.gray)
    }

    fun getCheckedId() = selectedId

    // randomize the questions and set the first question
    private fun randomizeQuestions() {
        questions.shuffle()
        questionIndex = 0
        setQuestion()
    }

    // Sets the question and randomizes the answers.  This only changes the data, not the UI.
    // Calling invalidateAll on the FragmentGameBinding updates the data.
    private fun setQuestion() {
        if(questionIndex == questions.size - 1) {
            nextBtn.visibility = View.INVISIBLE
            submitButton.visibility = View.VISIBLE
        }
        else {
            nextBtn.visibility = View.VISIBLE
            submitButton.visibility = View.INVISIBLE
        }

        if(questionIndex == 0)
            previousBtn.visibility = View.INVISIBLE
        else
            previousBtn.visibility = View.VISIBLE


        currentQuestion = questions[questionIndex]
        setCheckedId(currentQuestion.selectedAnswer)
        remainingTime = currentQuestion.remainingTime
        timerTxt.text = remainingTime.toString()
        progressBar.progress = remainingTime * 100 / 60
        questionsCounter.text = (questionIndex + 1).toString() + " / " + questions.size.toString()

        // randomize the answers into a copy of the array
        answers = currentQuestion.answers.toMutableList()

        if(remainingTime == 0)
            return

        countDownTimer = object : CountDownTimer((remainingTime * 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                remainingTime--
                timerTxt.text = remainingTime.toString()
                progressBar.progress = remainingTime * 100 / 60
                if (remainingTime == 0)
                    this.cancel()
            }

            override fun onFinish() {
            }
        }.start()

    }

    private lateinit var countDownTimer: CountDownTimer

}
