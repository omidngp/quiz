package com.example.quiz.model

data class ExamRequest(
    val id: Int,
    val answer: Int
)