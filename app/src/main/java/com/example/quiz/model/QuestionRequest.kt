package com.example.quiz.model

data class QuestionRequest(val count: Int)